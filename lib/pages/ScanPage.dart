import 'package:wallet/config/main.dart';
import 'package:flutter/material.dart';
import 'package:qr_mobile_vision/qr_camera.dart';
import 'package:wallet/controllers/StyleCtrl.dart';
import 'dart:async';

class ScanPage extends StatefulWidget {
  static const String routeName = '/Scan';

  @override
  State<StatefulWidget> createState() => new ScanPageState ();
}

class ScanPageState extends State<ScanPage> {
  StyleCtrl style = new StyleCtrl();
  bool isActive = true;

  @override
  Widget build(BuildContext context) {
    style.getSizes(context);

    return new Scaffold(
      backgroundColor: new Color.fromRGBO(255, 255, 255, 0.5),

      appBar: new AppBar(
        title: new Text('Scan QR Code'),
      ),
      body: new Center(
        child: new Container(
            foregroundDecoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("images/scanFrame.png",),
                fit: BoxFit.cover,
                colorFilter: new ColorFilter.mode(Colors.transparent, BlendMode.screen),
              ),
            ),
            child: isActive ? new QrCamera(
              fit: BoxFit.cover,
              qrCodeCallback: (code) {
                setState(() {
                  MainConfig.scannedData = new Future<String>(()=>code);
                  isActive = false;
                  Navigator.pop(context);
                });
              },
            ): new CircularProgressIndicator()
        ),
      ),
    );
  }
  
}