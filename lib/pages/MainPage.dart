import 'package:wallet/config/main.dart';
import 'package:wallet/controllers/HelperCtrl.dart';
import 'package:wallet/controllers/ServerCtrl.dart';
import 'package:wallet/controllers/StyleCtrl.dart';
import 'package:wallet/pages/SendFlow/ShowTXQRcodePage.dart';
import 'package:wallet/widgets/ActionButtonWidget.dart';
import 'package:wallet/widgets/GenerateWidget.dart';
import 'package:wallet/widgets/SpinnerWidget.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart';
import 'package:wallet/widgets/TransactionsWidget.dart';

class MainPage extends StatefulWidget {
  static const String routeName = '/Main';

  @override
  State<StatefulWidget> createState() => new MainPageState ();
}

class MainPageState extends State<MainPage> {

  StyleCtrl style = new StyleCtrl();
  GlobalKey<SpinnerWidgetState> _spinnerKey = new GlobalKey<SpinnerWidgetState>();
  Server server;

  MainPageState(){
    server = new Server(spinnerKey: _spinnerKey, setState: setState);
  }

  String msg = HelperCtrl.t("Loading...");
  String errorMsg = '';
  List<dynamic> wallets = [
    {"icon": 'images/crypto/btc.png', "title": "Bitcoin"},
    {"icon": 'images/crypto/eth.png', "title": "Ethereum"},
    {"icon": 'images/crypto/xmr.png', "title": "Monero"},
  ];


  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    GenerateWidget.startContext = context;
    style.getSizes(context);

    return new SpinnerWidget(new Scaffold(
      appBar: GenerateWidget.getAppBar(new Center(child: new Text(HelperCtrl.t("Title App")),), style, context),
        body:
          new Center(
            child: new Container(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Padding(padding: new EdgeInsets.all(5.0), child: new Text(HelperCtrl.t("Your Wallets"), style: style.getTitleStyle(color: Colors.grey, size: 30.0, isBold: true), textAlign: TextAlign.left,),),
                    new Container(
                      height: 100.0,
                      child: new ListView(
                        scrollDirection: Axis.horizontal,
                        children: wallets.map((item) => new Container(
                          margin: new EdgeInsets.all(10.0),
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            boxShadow: <BoxShadow>[
                              new BoxShadow (
                                color: Colors.grey,
                                offset: new Offset(1.0, 8.0),
                                blurRadius: 8.0,
                              ),
                            ],
                          ),
                          width: style.getOptimalSize(250.0),
                          height: style.getOptimalSize(150.0),
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Image.asset(item['icon']),
                              new Padding(padding: new EdgeInsets.all(10.0), child: new Text(item['title']),)
                            ],
                          ),
                        ),).toList(),
                      ),
                    ),

                    new Card(
                      elevation: 1.0,
                      child: new Column(crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Padding(padding: new EdgeInsets.all(5.0), child: new Icon(Icons.info_outline, color: Colors.grey,),),
                              new Text(HelperCtrl.t("Total Balance"), style: style.getTitleStyle(size: 30.0, color: Colors.grey, isBold: true),),
                            ],
                          ),

                          new Row(
                            children: <Widget>[
                              new Padding(padding: new EdgeInsets.all(5.0),
                                  child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Text("13.12345 BTC", style: style.getTitleStyle(color: Colors.blue,isBold: true, size: 50.0),),
                                      new Text("14.71 USD", style: style.getTitleStyle(color: Colors.grey,isBold: true, size:30.0)),
                                      new Text("12.12 EUR", style: style.getTitleStyle(color: Colors.grey,isBold: true, size: 30.0),),
                                    ],
                                  )
                              ),

                              new SizedBox(
                                width: style.width/2-8.0,
                                height: 100.0,
                                child: new LineChart(
                                  <Series<LinearSales, int>>[
                                    new Series(
                                      id: 'BTC',
                                      data:  [
                                        new LinearSales(0, 5),
                                        new LinearSales(1, 25),
                                        new LinearSales(2, 100),
                                        new LinearSales(3, 75),
                                        new LinearSales(4, 47),
                                        new LinearSales(5, 75),
                                        new LinearSales(6, 75),
                                        new LinearSales(7, 35),
                                        new LinearSales(8, 25),
                                      ],
                                      domainFn: (LinearSales sales, _) => sales.year,
                                      measureFn: (LinearSales sales, _) => sales.sales,
                                    )
                                  ],

                                  primaryMeasureAxis: new NumericAxisSpec(
                                      renderSpec: new SmallTickRendererSpec(
                                        // Tick and Label styling here.
                                      )),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),


                    new Padding(padding: new EdgeInsets.symmetric(vertical: 10.0), child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new ActionButtonWidget((){
                          MainConfig.amount = 0.12113322;
                          MainConfig.scannedData = null;
                          Navigator.pushNamed(context, ShowTXQRcodePage.routeName);
                        }, "Send", style),
                        new Padding(padding: new EdgeInsets.symmetric(horizontal: 10.0)),
                        new ActionButtonWidget(null, "Receive", style),
                      ],
                    ),),


                    new Padding(padding: new EdgeInsets.all(5.0), child: new Text(HelperCtrl.t("Transactions"), style: style.getTitleStyle(color: Colors.grey, size: 30.0, isBold: true), textAlign: TextAlign.left,),),

                    new Expanded(child: new TransactionsWidget(style: style,))
                  ],
                )
            ),
          )),
      msg, key: _spinnerKey,);
  }

}

class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}



