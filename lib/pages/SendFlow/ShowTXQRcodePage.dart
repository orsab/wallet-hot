import 'package:flutter/material.dart';
import 'package:wallet/config/main.dart';
import 'package:wallet/controllers/HelperCtrl.dart';
import 'package:wallet/controllers/StyleCtrl.dart';
import 'package:wallet/pages/ScanPage.dart';
import 'package:wallet/pages/SendFlow/ShowSignedTXQRcodePage.dart';
import 'package:wallet/widgets/ActionButtonWidget.dart';
import 'package:wallet/widgets/GenerateWidget.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:qr_reader/qr_reader.dart';

class ShowTXQRcodePage extends StatefulWidget {
  static const String routeName = '/ShowTXQRcodePage';

  @override
  State<StatefulWidget> createState() => new ShowTXQRcodePageState ();
}

class ShowTXQRcodePageState extends State<ShowTXQRcodePage> {
  StyleCtrl style = new StyleCtrl();
  Color color = Colors.black;

  String errorMsg = '';

  @override
  Widget build(BuildContext context) {
    style.getSizes(context);

    return new Scaffold(
      appBar: GenerateWidget.getAppBar(new Center(child: new Text(HelperCtrl.t("Sign Transaction")),), style, context),
      body: new Center(
        child: new Container(
          padding: new EdgeInsets.symmetric(horizontal: style.getOptimalSize(50.0)),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(HelperCtrl.t("Scan with Absolute Zero"), textAlign:TextAlign.center,style: style.getTitleStyle(isBold: true)),

              new Padding(padding: new EdgeInsets.all(20.0)),

              new Text(MainConfig.amount.toStringAsFixed(8) + ' BTC', textAlign:TextAlign.center,style: style.getTitleStyle(isBold: true, color: Colors.blue, size: 60.0)),

              new Text(MainConfig.TXIDnotsigned, textAlign:TextAlign.center,softWrap: false,style: style.getTitleStyle(color: Colors.grey)),

              new Text(errorMsg, style: style.getTitleStyle(color: Colors.red),),

              new FutureBuilder(builder: (BuildContext con, AsyncSnapshot<String> snapshot){
                if(snapshot.data != null) {
                  dynamic data = HelperCtrl.parseQR(snapshot.data);

                  if (data != null) {
                    if (data['result'] == 'ok') {
                      MainConfig.TXIDsigned = data['data'];

                      return new Padding(
                        padding: new EdgeInsets.all(20.0), child: new QrImage(
                        foregroundColor: Colors.green,
                        data: MainConfig.TXIDsigned,
                        size: style.getOptimalSize(400.0),
                      ),);
                    }
                    else {
                      setState(() {
                        errorMsg = 'Try again';
                      });

                      return new Padding(
                        padding: new EdgeInsets.all(20.0),
                        child: new QrImage(
                          foregroundColor: Colors.red,
                          data: MainConfig.TXIDnotsigned,
                          size: style.getOptimalSize(400.0),
                        ),
                      );
                    }
                  }
                  else {
                    MainConfig.TXIDsigned = null;
                    return new Padding(
                        padding: new EdgeInsets.all(50.0),
                        child: new Text("Wrong data received",
                          style: style.getTitleStyle(color: Colors.red),)
                    );
                  }
                }
                else{
                  return new Padding(
                    padding: new EdgeInsets.all(20.0),
                    child: new QrImage(
                      foregroundColor: Colors.black,
                      data: MainConfig.TXIDnotsigned,
                      size: style.getOptimalSize(400.0),
                    ),
                  );
                }
              }, future: MainConfig.scannedData,),

              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new FutureBuilder(
                      future: MainConfig.scannedData,
                      builder: (BuildContext con, AsyncSnapshot<String> snapshot){
                          if(snapshot.data == null || MainConfig.TXIDsigned == null){
                            return new ActionButtonWidget(() async {
                              Navigator.pushNamed(context, ScanPage.routeName);
                            }, "Scan", style);
                          }
                          else
                            return new ActionButtonWidget(() async {
                              Navigator.pop(context);
                            }, "Next", style);
                      })
                  )
                ]
              )
//                      else{
//                        setState(()=>MainConfig.TXIDsigned = null);
//                        Navigator.pop(context);
//                      }
//      }, MainConfig.TXIDsigned==null ? "Scan" : "Next", style),)
//    }
//      })
//                    new ActionButtonWidget(() async {
//
//                      if(MainConfig.TXIDsigned==null){
//                        Navigator.pushNamed(context, ScanPage.routeName);
//                        return;
//                      }
//                      else{
//                        setState(()=>MainConfig.TXIDsigned = null);
//                        Navigator.pop(context);
//                      }
//                  }, MainConfig.TXIDsigned==null ? "Scan" : "Next", style),)
//                ],
//              )

            ],
          ),
        ),
      ),
    );
  }
  
}