import 'package:flutter/material.dart';
import 'package:wallet/config/main.dart';
import 'package:wallet/controllers/HelperCtrl.dart';
import 'package:wallet/controllers/StyleCtrl.dart';
import 'package:wallet/widgets/ActionButtonWidget.dart';
import 'package:wallet/widgets/GenerateWidget.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:qr_reader/qr_reader.dart';

class ShowSignedTXQRcodePage extends StatefulWidget {
  static const String routeName = '/ShowSignedTXQRcodePage';

  @override
  State<StatefulWidget> createState() => new ShowSignedTXQRcodePageState ();
}

class ShowSignedTXQRcodePageState extends State<ShowSignedTXQRcodePage> {
  StyleCtrl style = new StyleCtrl();
  Color color = Colors.black;

  String errorMsg = '';

  @override
  Widget build(BuildContext context) {
    style.getSizes(context);

    return new Scaffold(
      appBar: GenerateWidget.getAppBar(new Center(child: new Text(HelperCtrl.t("Transaction Signed")),), style, context),
      body: new Center(
        child: new Container(
          padding: new EdgeInsets.symmetric(horizontal: style.getOptimalSize(50.0)),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(HelperCtrl.t("Transaction Signed"), textAlign:TextAlign.center,style: style.getTitleStyle(isBold: true)),
              new Padding(padding: new EdgeInsets.all(20.0)),

              new Text(MainConfig.amount.toStringAsFixed(8) + ' BTC', textAlign:TextAlign.center,style: style.getTitleStyle(isBold: true, color: Colors.blue, size: 60.0)),

              new Text(MainConfig.TXIDsigned, softWrap: false, textAlign:TextAlign.center,style: style.getTitleStyle(color: Colors.grey)),

              new Padding(padding: new EdgeInsets.all(20.0), child: new QrImage(
                foregroundColor: Colors.green,
                data: MainConfig.TXIDsigned,
                size: style.getOptimalSize(400.0),
              )),

              new Padding(padding: new EdgeInsets.all(10.0), child: new Text(errorMsg, style: style.getTitleStyle(color: Colors.red),),),

              new ActionButtonWidget(() {
                Navigator.pop(context);
              }, "Done", style),

            ],
          ),
        ),
      ),
    );
  }
  
}