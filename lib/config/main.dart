import 'dart:async';

enum AppLocale {EN}

class MainConfig{
  static bool isProduction = true;

  static AppLocale locale = AppLocale.EN;

  static String endpointURL = '';

  static String version = '1.0.0';

  static String TXIDnotsigned = 'b06c51fababf70f247a21bb95e1d9fa778f1bc1b2a38ac71b82baa6af66fce11';

  static String TXIDsigned;

  static double amount = 0.0;

  static Future<String> scannedData;
}