import 'package:flutter/material.dart';
import 'package:wallet/controllers/StyleCtrl.dart';

class ActionButtonWidget extends StatefulWidget {
  ActionButtonWidget(this.onClick(), this.title, this.style, {color: Colors.blue, width: 150.0}){
    this.color = color;
    this.width = width;
  }

  @override
  State<StatefulWidget> createState() => new ActionButtonWidgetState();

  final dynamic onClick;
  final String title;
  Color color;
  double width;
  final StyleCtrl style;

}

class ActionButtonWidgetState extends State<ActionButtonWidget> {
  @override
  Widget build(BuildContext context) {

    return new RaisedButton(
        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
        color: Colors.blue,
        padding: new EdgeInsets.all(15.0),
        onPressed: super.widget.onClick,

        child: new Container(
          width: super.widget.width,
          child: new Text(
            super.widget.title,
            style: super.widget.style.getTitleStyle(color: Colors.white),
            textAlign: TextAlign.center,
          )
        )
    );
  }
}