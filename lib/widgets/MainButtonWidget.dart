import 'dart:async';
import 'dart:convert';

import 'package:wallet/controllers/PageCtrl.dart';
import 'package:wallet/controllers/ServerCtrl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class MainButtonWidget extends StatefulWidget {
  MainButtonWidget(this.onClick(), this.title, this.icon, this.color, this.ratio);

  @override
  State<StatefulWidget> createState() => new MainButtonWidgetState();

  final dynamic onClick;
  final String title;
  final String icon;
  final Color color;
  final double ratio;

}

class MainButtonWidgetState extends State<MainButtonWidget> {
  @override
  Widget build(BuildContext context) {


    return new Container(
        decoration: new BoxDecoration(
          borderRadius: new BorderRadius.circular(25.0),
          border: new Border.all(
            width: 3.0,
            color: super.widget.color,
          ),
        ),
        child: new MaterialButton(
          onPressed: super.widget.onClick,
          child: new Container(
            child: new Column(
              children: <Widget>[
                new Padding(padding: new EdgeInsets.all(super.widget.ratio*20.0),child: new Image.asset(super.widget.icon, height: super.widget.ratio*100.0,),),
                new Text(super.widget.title, style: new TextStyle(fontSize: super.widget.ratio * 40.0, color: Colors.white),textAlign: TextAlign.center),
                new Padding(padding: new EdgeInsets.all(10.0),),
              ],
            ),
          ),
        )
    );
  }
}