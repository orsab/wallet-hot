import 'package:wallet/controllers/HelperCtrl.dart';
import 'package:wallet/controllers/StyleCtrl.dart';
import 'package:flutter/material.dart';

class TextInputWidget extends StatefulWidget{
  final TextEditingController ctrl;
  final StyleCtrl style;
  String helperText;
  String currency;
  bool isMandatory;
  TextInputType inputType;

  TextInputWidget(this.ctrl, this.style, {String helperText='', bool isMandatory = false,TextInputType inputType=TextInputType.number, String currency }){
    this.helperText = helperText;
    this.isMandatory = isMandatory;
    this.inputType = inputType;
    this.currency = currency;
  }

  @override
  State<StatefulWidget> createState() => new TextInputState();

}

class TextInputState extends State<TextInputWidget> {
  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: new EdgeInsets.symmetric(horizontal: widget.style.isFront ? widget.style.getOptimalSize(60.0) : widget.style.getOptimalSize(90.0), vertical: widget.style.getOptimalSize(5.0)),
      child: new Directionality(textDirection: widget.style.isRtl ? TextDirection.rtl : TextDirection.ltr, child: new TextField(
        textAlign: widget.style.isRtl ? TextAlign.right : TextAlign.left,
        controller: widget.ctrl,
        keyboardType: widget.inputType,
        decoration: new InputDecoration(
            fillColor: Colors.white,
            filled: true,
            hintText: HelperCtrl.t(widget.helperText),
            hintStyle: new TextStyle(fontSize: widget.style.getOptimalSize(30.0))
        ),
        style: new TextStyle(fontSize: widget.style.getOptimalSize(30.0),
            decoration: TextDecoration.none),
      )),
    );
  }


}