import 'package:wallet/config/main.dart';
import 'package:flutter/material.dart';

class StyleCtrl{
  double _width;
  double _height;
  double ratio;
  bool isFront;

  bool get isRtl => false;

  double get width => _width;
  double get height => _height;

  getSizes(BuildContext context){
    _width = MediaQuery.of(context).size.width;
    _height = MediaQuery.of(context).size.height;
    ratio = 0.0;

    if(_height>_width){
      isFront = true;
      ratio = _height / 1200;
    }
    else{
      isFront = false;
      ratio = _width / 1600;
    }


    return ratio;
  }

  static Color getPrimaryColor() => new Color.fromRGBO(80, 73, 44, 0.83) ;

  getOptimalSize(double size) {
    return size * ratio;
  }

  TextStyle getTitleStyle({double size=40.0, isBold=false, Color color}) {
    return new TextStyle(
        color: color != null ? color : getPrimaryColor(),
        fontSize: getOptimalSize(size),
        fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
    );
  }

  getTextDirection() {
    if(isRtl)
      return TextDirection.rtl;
    else
      return TextDirection.ltr;
  }
}