import 'dart:convert';

import 'package:wallet/config/main.dart';
import 'package:wallet/locales/en.dart';
import 'package:flutter/material.dart';

class HelperCtrl{
  final RegExp btcExp = new RegExp('^[13][a-km-zA-HJ-NP-Z0-9]{26,33}\$');
  GlobalKey<ScaffoldState> _scaffold;

  HelperCtrl();

  void setScaffoldKey(GlobalKey<ScaffoldState> scaffold){
    _scaffold = scaffold;
  }

  bool isBtcAddress(String address){
    return btcExp.hasMatch(address);
  }

  void logData(data, {String tag}){
    print(tag.isNotEmpty ? tag + ': ' +JSON.encode(data) : '' + JSON.encode(data));
  }

  void showSnack(String msg) {
    _scaffold.currentState.showSnackBar(new SnackBar(
      content: new Text(msg,
        style: new TextStyle(fontSize: 40.0, fontStyle: FontStyle.italic),),
      duration: new Duration(seconds: 1),));
  }

  static String t(String source){
    String result = "";
    switch(MainConfig.locale){
      case AppLocale.EN:
        return lang_en.containsKey(source) ? lang_en[source] : source;
    }

    return result;
  }

  static String tp(String str, Map<String,String> params){
    String trans = t(str);

    params.forEach((String k, String v) => trans = trans.replaceFirst(k, v));
    return trans;
  }

  static parseQR(String barcodeString) {
    dynamic resp;
    try {
      resp = json.decode(utf8.decode(base64.decode(barcodeString)));
    }catch(e){
      print(e.toString());
      return null;
    }
    return resp;
  }

}