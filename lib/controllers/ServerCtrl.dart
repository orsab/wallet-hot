import 'package:wallet/config/main.dart';
import 'package:wallet/controllers/HelperCtrl.dart';
import 'package:wallet/widgets/SpinnerWidget.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class Server {
  final bool isProdaction = MainConfig.isProduction;
  String mainServerAPI;
  HelperCtrl helper = new HelperCtrl();
  Completer<http.Response> completer;

  GlobalKey<SpinnerWidgetState> spinnerKey;

  var setState;

  Server({this.spinnerKey = null, this.setState}){
    mainServerAPI = isProdaction ? MainConfig.endpointURL : '';
  }

  switchSpinner(bool isOn){
//    return;
    if(spinnerKey != null){
      setState(() => spinnerKey.currentState.active = isOn);
    }
  }

  Future<http.Response> postData(dynamic params, {String postfix=''}) async {
    helper.logData(params, tag: 'Post params '+postfix);
    if(!postfix.contains('order'))
      switchSpinner(true);

    if(!isProdaction){
      Completer<http.Response> completer = new Completer<http.Response>();

      return completer.future;
    }

    String endPoint = postfix.isNotEmpty ? mainServerAPI + postfix : mainServerAPI;

    http.Response resp = await http.post(endPoint,
        body: JSON.encode(params),
        headers: {"Content-type": "application/json"}
    );
    switchSpinner(false);

    helper.logData(resp.body, tag: 'Post response '+postfix);

    return resp;
  }

  Future<http.Response> getData({String postfix=''}) async {
    helper.logData(postfix, tag: 'Get params');

    if(!postfix.contains('order'))
      switchSpinner(true);

    if(!isProdaction){
      Completer<http.Response> completer = new Completer<http.Response>();

      return completer.future;
    }

    String endPoint = postfix.isNotEmpty ? mainServerAPI + postfix : mainServerAPI;

    http.Response resp = await http.get(endPoint,
        headers: {"Content-type": "application/json"}
    );
    switchSpinner(false);

    helper.logData(resp.body, tag: 'Get response '+postfix);

    return resp;
  }

}
